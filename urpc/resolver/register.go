package resolver

import (
	"gitee.com/go-ultra/gou/urpc/resolver/internal"
)

// Register registers schemes defined urpc.
// Keep it in a separated package to let third party register manually.
func Register() {
	internal.RegisterResolver()
}
