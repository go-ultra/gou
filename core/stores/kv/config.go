package kv

import "gitee.com/go-ultra/gou/core/stores/cache"

// KvConf is an alias of cache.ClusterConf.
type KvConf = cache.ClusterConf
