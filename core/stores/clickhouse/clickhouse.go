package clickhouse

import (
	// imports the driver, don't remove this comment, golint requires.
	"gitee.com/go-ultra/gou/core/stores/sqlx"
	_ "github.com/ClickHouse/clickhouse-go"
)

const clickHouseDriverName = "clickhouse"

// New returns a clickhouse connection.
func New(datasource string, opts ...sqlx.SqlOption) sqlx.SqlConn {
	return sqlx.NewSqlConn(clickHouseDriverName, datasource, opts...)
}
